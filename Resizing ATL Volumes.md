Resizing ATL Partitions
=======================

`/dev/sda3` on the physical ATL machines (not the remote pool) has a ridiculously big empty LVM partition called `/home.orig`, which is taking up space needed by `/`

1. Unmount `/home.orig`
2. Reduce the home LVM partition
3. Extend the root LVM partition
4. Grow the file system on root

To do this, first comment out line 12 in `/etc/fstab` to prevent `/home.orig` being mounted in future, then execute the following as root

```
umount /home.orig
lvreduce -f -L -100G /dev/scientific/home
lvextend -L +100G /dev/scientific/root
xfs_growfs /

```

0001: Timeout
0014: not known
0045: Timeout



:12
I# 
:wq
