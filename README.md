Mindhive
========

A place to put documentation, todos, roadmaps, and similar braindumps.

ToDo
----

* [spm-lab.ppls.ed.ac.uk instructions](./spm-lab todo.md)
* Lion disk space recovery
* Shiny Server setup
* LBC Synology &rarr; Datastore migration
* Gallowglass3 CUDA drivers
* Neurodesk build
* Service catalogue
* CVE patching for `xz` vuln

Docs
----

* [Resizing ATL Volumes](./Resizing ATL Volumes.md)

Roadmap
-------

&rarr; [Linux server/client roadmap](./Linux roadmap.md)
