spm-lab-1 todo
==============

This is one of the machines in 7GS B20 - the one nearer the door, with two green dots on the front.

Register new machine in DDI
---------------------------

* Name `spm-lab-1.ppls.ed.ac.uk`
* MAC is `98:90:96:9c:3a:0a`
* Register to 7gs network
* Dell Precision 7910 running Rocky Linux 9

Decomission old machine
-----------------------

* Replace one box with the other
* Remove old machine from DDI
  * Optionally attack machine with axe
  * A sledgehammer would also work
* Kick it to CCL

Create account for Mark Bastin
------------------------------

* With him there so he can set the password
* Make him an admin (so he has sudo rights rather than needing the root pw)

Notes
-----

* Root account is enabled and turned on for ssh, password is in lastpass
* Hostname is already set on machine
