Linux Infrastructure Roadmap
============================

Things we need to do for the School's linux infrastructure, covering security, monitoring, configuration, etc.

Service Catalogue
-----------------

We need to know what services we offer. This includes:

* Servers, including whether physical or virtual
* OS versions and patch status
* What each server does
* Who needs access to each server

Authentication & Authorisation
------------------------------

Join servers to AD

* Use AD groups to control access to services
* Allows IT staff to automatically be admins/sudoers

Kerberos integration

* Means we don't need passwords all the time
* Kerberized ssh means not worrying about ssh keys etc.
* Does require getting & maintaining keytabs

Build
-----

Standardising builds and server config.

* Straightforward set of apps
* Straightforward and automatic setting of hostname etc.
* Automatically setting keytabs etc.

Config Management
-----------------

This is basic stuff for security and related stuff, including setting up things for A&A and Build steps as well as software & monitoring setups.

* Set up physical server (salt/ansible for preference)

Monitoring
----------

So we know what's up and what's not

* It's probably going to have to be nagios, not because it's good but because everything else is worse.